![AQI LOGO](https://sit.aqi.co.id/img/logo_aqi.jpg)

# ZegoCloud Flutter

## Getting Started
This is an resource which implemented from ZegoCloud SDK Video Call with Flutter framework.

### Feature
1. Basic video call
2. Video call invitation

### How to running the app?
Before running the app, please follow in following steps:
1. Create `.env` file in root directory 
2. Add global variables 
```
APP_ID = Your AppId 
APP_SIGN  = Your AppSign
SERVER_SECRET = Your Server Secret
```
3. Run `flutter pub get` to update new file 
